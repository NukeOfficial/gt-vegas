import aioredis
import aiopg

async def setup_redis(bot):
    bot.redis = await aioredis.create_redis_pool('redis://localhost')

async def setup_postgres_pool(bot):
    bot.pg_pool = await aiopg.create_pool(POSTGRES_AUTH)

DEBUG = False

# Prefix used for bot commands
BOT_PREFIX = ("v?", "v!")

# Default disord.GUILD.id to use
GUILD_ID = 353694095220408322

COLORS = {
        'ban': 7821292, # Snap Purple
        'unban': 13429847,
        'edit': 15538157,
        'delete': 14487859,
        'voice': 1904757,
        'role_removed': 16759569,
        'role_added': 4513245,
        'join': 15663086,
        'left': 1114129,
        'profile_change': 14318337,
        'username': 14318337,
        'avatar': 14318337,
    }

# What plugins to load from the `plugins` directory
COGS = (
    'plugins.notifications',
    'plugins.loaders',
    'plugins.messages',
    'plugins.utilities',
    'plugins.among_us',
    'plugins.fun',
)

# The number of messages discord.py stores in memory
MAX_MESSAGES = 5000

# Discord Token for bot authentication
TOKEN = ''

POSTGRES_AUTH = 'dbname=<db_name> user=<db_user> password=<db_password> host=127.0.0.1'

NOTIFICATION_DICT = {
    "Game Theory Notification": "<:GameTheory:695120563991216178>",
    "Film Theory Notification": "<:FilmTheory:695120565081735208>",
    "Food Theory Notification": "<:FoodTheory:735996033711997029>",
    "GTLive Notification": "<:GTLive:695120565236662312>",
    "GTARG Notification": "<:tenretni:776452483823304745>",
    "Theory Competition Notification": "<:TheoryTheory:735924393397583995>",
    "Art Competition Notification": "🎨",
    "Writing Competition Notification": "📜",
    "Music Competition Notification": "🎼",
    "Meme Contest Notification": "<:SansIsNess:695120564318109816>",
    "Question of the Day Notification": "<:QuestionBlock:695120559377350696>",
    "Community Events Notification": "<:CWStamp:731523543325737070>",
    "AT Puzzle Notification": "🧩",
    "Writing Prompt Notification": "✍️",
    "Music Event Notification": "🎶",
    "Meme Tournament Notification": "<:SansXD:695120561453400076>",
    "Cooking Event Notification": "🧑‍🍳",
    "Book Recs Notification": "<:BookTheory:721427272510210059>",
    "Resolution Checkup Notification": "<:SweatPat:695120565253439490>",
    "Trello Staff Notification": "<:emodseal:695120556046942238>",
    "Volunteer Notification": "<:SuperStar:695120560258285690>",
    "Community ARG Notification": "<:sus_think:695120561352736870>",
    "Stream Notification": "<:kiwipopcorn:695120561902321694>",
    "VC Music Notification": "🎤",
    "Among Us Game Notification": "<:AmongPat:759778089298690099>",
    "Duck Game Switch Notification": "🦆",
    "jjba it works Notification": "<:pogsuke:695120565744435200>",
    "professor asters coding 101 Notification": "<:CoolAsterWithFingerGunsWowww:695120559612231771>",
    "Chop Suey Pehp Sway Notification": "🦎",
    "Ask Pronouns": "💬",
    "He/Him": "♂️",
    "She/Her": "♀️",
    "They/Them": "⚧",
}

ROLE_PERMS = [
    {
        "name": "Divine Theorist",
        "perms": "•**Prestige?** You can't prestige until you reach <@&514115149158678550>. \n"
    },
    {
        "name": "Ascended Theorist",
        "perms": []
    },
    {
        "name": "Sage Theorist",
        "perms": "•**Use !degendex?** You can't use !degendex until you reach <@&514115324841426954>. \n"
    },
    {
        "name": "Pure Theorist",
        "perms": []
    },
    {
        "name": "Legendary Theorist",
        "perms": "•**Go Live?** You can't Go Live in a VC until you reach <@&514115410396839955>. \n"
    },
    {
        "name": "Limitless Theorist",
        "perms": "•**Post in warn reports?** You can't post in <#624257664125501450> until you reach <@&514115613468393473>. \n"
    },
    {
        "name": "Inhuman Theorist",
        "perms": []
    },
    {
        "name": "Master Theorist",
        "perms": "•**Use !sleeping-chat?** You can't use !sleeping-chat until you reach <@&514115743151816714>. \n"
    },
    {
        "name": "Genius Theorist",
        "perms": []
    },
    {
        "name": "Enlightened Theorist",
        "perms": []
    },
    {
        "name": "Insane Theorist",
        "perms": "•**See the mod preview channel?** You can't see <#519873222506709002> until you reach <@&514116251723890732>. \n"
    },
    {
        "name": "Obsessed Theorist",
        "perms": "•**Post in daily positivity?** You can't post in <#565566736800284683> until you reach <@&514116287283331073>. \n"
    },
    {
        "name": "Addicted Theorist",
        "perms": []
    },
    {
        "name": "Dedicated Theorist",
        "perms": [
            "•**Be recommended for Approved roles?** You can't be recommended for Approved roles until you reach <@&514116355763732480>. \n",
        ]
    },
    {
        "name": "Loyal Theorist",
        "perms": "•**Send suggestions?** You can't suggest in the Suggestions channels until you reach <@&514116391607992332>. \n"
    },
    {
        "name": "Serious Theorist",
        "perms": "•**Change my nickname?** You can't change your nickname until you reach <@&514114232011194379>. \n"
    },
    {
        "name": "Hard-Working Theorist",
        "perms": [
            "•**Attach files?** You can't attach files (outside of <#354405657484460043>, <#528253600682737685>, <#353766146115371009> and the Theorizing channels) until you reach <@&514114175409061900>. \n",
            "•**Embed links?** You can't embed links (outside of <#354405657484460043>, <#528253600682737685>, <#353766146115371009> and the Theorizing channels) until you reach <@&514114175409061900>. \n",
            "•**See suggestion channels?** You can't see the Suggestions channels until you reach <@&514114175409061900>. \n",
        ]
    },
    {
        "name": "Improving Theorist",
        "perms": [
            "•**Use emojis from other servers?** You can't use external emoji until you reach <@&514114132392411157>. \n",
            "•**Use !yeet?** You can't use !yeet until you reach <@&514114132392411157>. \n",
        ]
    },
    {
        "name": "Rookie Theorist",
        "perms": [
            "•**Add reactions?** You can't add reactions until you reach <@&514114064608264205>. \n",
            "•**Send images?** You can only send images in the Theorizing Channels once you reach <@&514114064608264205>. \n"
        ]
    },
    {
        "name": "New Theorist",
        "perms": "•**Send images?** You can only send images in <#353766146115371009> and <#528253600682737685> once you reach <@&514113781903917056>. \n"
    },
]
APPROVED_ROLES = ['Mods', 'Server Glue', 'Approved Theorizer', 'Approved Artist',
        'Approved Writer', 'Approved Musician', 'Expert Meme-er',]

POSTGRES_ERR = "Please ensure Postgres and aiopg are installed; and local_settings.setup_postgres_pool and POSTGRES_AUTH are set"
REDIS_ERR = "Please ensure Redis and aioredis are installed and local_settings.setup_redis is defined!"
