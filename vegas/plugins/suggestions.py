import asyncio
from functools import partial

import discord
from discord.ext import commands
from discord.utils import get

from settings.base import COLORS
from plugins.base import BasePlugin
"""
SuggestionsPlugin

"""
TIMEOUT = 120.0

REQUIRED_ROLE = 'Loyal Theorist'
REVIEW_CHANNEL_NAME = 'approved-feedback'
SUGGESTION_CHANNEL = 'suggestions-and-community-support'
SUGGESTION_INSERT = "INSERT INTO suggestions_suggestion (suggestion, attachment, submitted, discord_user_id, approved) VALUES (%s, %s, %s, %s,  %s)"
SUGGESTION_SELECT = "SELECT * FROM suggestions_suggestion WHERE discord_user_id = %s ORDER BY submitted DESC"
SUGGESTION_GET = "SELECT * FROM suggestions_suggestion WHERE id = %s"
SUGGESTION_UPDATE = "UPDATE suggestions_suggestion SET approved='t' WHERE id = %s;"

class SuggestionPlugin(BasePlugin):
    def __init__(self, bot):
        self.bot = bot

    async def _generate_suggestion_embed(self, submission, author, guild, submission_date, attach_url, sid=None):
        embed = discord.Embed(title="New Suggestion", color=COLORS['ban'], description=submission)
        embed.set_author(name=author.name + author.discriminator, icon_url=author.avatar_url)
        embed.set_thumbnail(url=guild.icon_url)
        embed.add_field(name="Author:", value=author.mention, inline=True)
        embed.add_field(name="Submission Date:", value=submission_date, inline=True)

        if sid:
            embed.add_field(name="ID:", value=id)

        if attach_url:
            embed.set_image(url=attach_url)
        return embed

    @commands.group(description="Have a suggestion?",
                    case_insensitive=True)
    async def suggestion(self, context):
        if context.invoked_subcommand is None:
            await context.send('What do you want to do with Suggestions?')

    @suggestion.command(description="Add a suggestion for Staff Review!")
    @commands.dm_only()
    async def c(self, context, *submission):
        submission = " ".join(submission)
        await context.send(submission)

    @suggestion.command(description="Add a suggestion for Staff Review!",
                        aliases=['a', '+'])
    @commands.dm_only()
    async def add(self, context, *submission):
        guild = await self._get_guild(context)
        author = guild.get_member(context.message.author.id)
        link_id = await self._check_user_exists(author.id, guild.id)
        submission = " ".join(submission)
        submission_date = context.message.created_at.strftime("%b %d %y")
        required_role = get(guild.roles, name=REQUIRED_ROLE)
        attach_url = ""
        if context.message.attachments:
            attach_url=context.message.attachments[0].url

        if not submission:
            await context.send("Please make sure to include a suggestion!")
            return
        if len(submission) > 2048:
            await context.send("Discord can only accept 2048 characters, please send a Google doc or images to help explain.")

        if not author.top_role.position >= required_role.position:
            await context.send("You don't have a high enough role to add a suggestion. Please check out #server-info (https://discord.com/channels/353694095220408322/735635802960429087/735639337613656086) for more information.")
            return

        embed = await self._generate_suggestion_embed(submission, author, guild, submission_date, attach_url)

        review_message = await context.send(content="React with 👍 to send or 👎 to cancel.", embed=embed)

        def _check_is_author(reaction, user):
            return user == context.author

        while True:
            reaction, reaction_user = await self.bot.wait_for('reaction_add', timeout=TIMEOUT, check=_check_is_author)
            if reaction.emoji == "👍":
                async with self.bot.pg_pool.acquire() as conn:
                    async with conn.cursor() as cursor:
                        # Creates the new record
                        await cursor.execute(SUGGESTION_INSERT, (submission, attach_url, submission_date, link_id, False))
                        await cursor.execute(SUGGESTION_SELECT, (link_id,))
                        suggestions = await cursor.fetchall()
                await context.send("Thanks for the suggestion! Your suggestion id is {}".format(suggestions[0][0]))
                embed.add_field(name="ID:", value=suggestions[0][0])
                af = get(guild.text_channels, name=REVIEW_CHANNEL_NAME)
                af_message = await af.send(embed=embed)
                await af_message.pin()
                return
            elif reaction.emoji  == "👎":
                await review_message.edit(content="Sorry to hear that. If you have any suggestions send them to us!", embed=None)
                return
            else:
                #await embed_message.clear_reactions()
                warning = await context.send("Please react with 👍 or 👎")
                await warning.delete(delay=5)


    @add.error
    async def add_error(self, context, error):
        if isinstance(error, commands.PrivateMessageOnly):
            await context.send("You can only do that in a DM! The guild id you need is {}".format(
                    context.guild.id
                ))
        if isinstance(error, asyncio.TimeoutError):
            warning = await context.send("Please react with 👍 or 👎")
            await warning.delete(delay=5)
            return

    @commands.Cog.listener()
    @commands.guild_only()
    async def on_raw_reaction_add(self, payload):
        guild = self.bot.get_guild(payload.guild_id)
        channel = self.bot.get_channel(payload.channel_id)
        author = guild.get_member(payload.user_id)

        if str("🆗") != str(payload.emoji):
            return
        if isinstance(channel, discord.DMChannel):
            return
        if channel.name != REVIEW_CHANNEL_NAME:
            return
        if not any("mods" == role.name.lower() for role in author.roles):
            await channel.send("Only mods can approve suggestions with 🆗!")
            return

        message = await channel.fetch_message(payload.message_id)
        embed = message.embeds[0]
        embed_dict = embed.to_dict()
        suggestion_id = int(embed_dict['fields'][2]['value'])

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cursor:
                await cursor.execute(SUGGESTION_UPDATE, (suggestion_id,))
                await channel.send("{} approved suggestion #{}".format(author.mention, suggestion_id))
        suggestion_channel = get(guild.text_channels,name=SUGGESTION_CHANNEL)
        embed.remove_field(2)
        await suggestion_channel.send(embed=embed)


def setup(bot):
    bot.add_cog(SuggestionPlugin(bot))
