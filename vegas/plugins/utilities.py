import asyncio
import discord
from discord.utils import get
from discord.ext import commands
from discord.ext.commands import has_any_role
from discord import ActivityType

from settings.base import ROLE_PERMS
from .base import BasePlugin
from discord.ext.commands import is_owner
import re

LOG_CHANNEL_NAME = 'vegas-logging'

APPROVED_ROLES = [
    "Mods",
    "Disaster Director",
    "Server Glue",
    "Approved Theorizer",
    "Approved Artist",
    "Approved Writer",
    "Approved Musician",
    "Expert Meme-er",
]

pattern = re.compile(r'''
            \bn\W*[i*](i|\*|\W)*\W*[gb](g|b|\W)*(?!el|eria)(ers*|a|s|\W)*\b|
            \bs\W*p\W*[i*]+\W*c\W*(s|\W)*\b|
            \bf\W*[a*]+\W*[g]+\W*(got|\W)*(s|\W)*\b|
            \bc\W*[u*]+\W*m\W*s*\b|
            \bc\W*[u*]+\W*n\W*t\W*(s|\W)*\b|
            \bc\W*[o0*]\W*c\W*k\W*s*\b|
            \bp\W*u\W*[s$*]\W*[s$*]\W*(y|i|e|\W)+(s|\W)*\b|
            \bb\W*l\W*[o*]\W*w\W*j\W*o\W*b\W*(s|\W)*\b|
            \bc\W*h\W*[i*]\W*n\W*k\W*(s|\W)*\b|
            \bk\W*[i*]\W*k\W*e\W*(s|\W)*\b|
            \br*\W*[e*]*\W*t\W*[a*]\W*[r*]\W*d(ed|s|\W)*\b
            ''', re.X | re.I)

everyone_pattern = re.compile(r'@everyone')

BAN_APPEAL = 'https://bit.ly/GTD-Ban-Appeal'

DATE_FORMAT = "%a, %b %-d, %Y @ %-I:%M %p"

WARN_COUNT_SQL = "select count(w.id) FROM discord_user_discorduser as u LEFT JOIN discord_user_discordwarn AS w ON u.id = w.discord_user_id WHERE u.discord_user_id=%s"

STRK_COUNT_SQL = "select count(s.id) FROM discord_user_discorduser as u LEFT JOIN discord_user_discordstrike AS s ON u.id = s.discord_user_id WHERE u.discord_user_id=%s"


class UtilitiesPlugin(BasePlugin):
    def event_logging_channel(self, channels):
        return get(channels, name=LOG_CHANNEL_NAME)

    @commands.command(description="Update bot activity",
                      case_insensitive=True)
    @is_owner()
    async def status(self, context: discord.ext.commands.Context, status_type: str, status_url: str,
                     *status_message: str) -> None:
        """Updates the bot status"""
        status_message = " ".join(status_message)
        lst = status_type.lower()
        if "watching" in lst:
            await self.bot.change_presence(activity=discord.Activity(type=ActivityType.watching, name=status_message))
        elif "streaming" in lst:
            await self.bot.change_presence(activity=discord.Streaming(name=status_message, url=status_url))
        elif "gaming" in lst:
            await self.bot.change_presence(activity=discord.Game(name=status_message))
        elif "listening" in lst:
            await self.bot.change_presence(
                activity=discord.Activity(type=discord.ActivityType.listening, name=status_message))
        else:
            await self.bot.change_presence(activity=discord.Activity(name=status_message))

    @commands.command(description="List information about the user.",
                      case_insensitive=True)
    async def info(self, context, username=None):
        member = context.message.author
        if username:
            member = context.message.mentions[0]
        color = member.top_role.color
        desc = "All the infos"
        embed = discord.Embed(description=desc, color=color)

        author = "{}#{}".format(member.name, member.discriminator)
        embed.set_author(name=author, icon_url=member.avatar_url)
        embed.set_thumbnail(url=member.avatar_url)
        embed.add_field(name="User ID:", value=member.id)
        embed.add_field(name="User Nickname:", value=member.nick)

        premium_status = "[Not Boosted]"
        if member.premium_since:
            premium_status = member.premium_since.strftime(DATE_FORMAT)
        embed.add_field(name="Boosting Since:", value=premium_status, inline=False)

        embed.add_field(name="Account Created:", value=member.created_at.strftime(DATE_FORMAT))
        embed.add_field(name="Joined Server:", value=member.joined_at.strftime(DATE_FORMAT))

        embed.add_field(name="Server Information", value="-----------------------", inline=False)

        async with self.bot.pg_pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(WARN_COUNT_SQL, (member.id,))
                warn_count = await cur.fetchone()
                await cur.execute(STRK_COUNT_SQL, (member.id,))
                strike_count = await cur.fetchone()

        embed.add_field(name="Warn Count:", value=warn_count[0])
        embed.add_field(name="Strike Count:", value=strike_count[0])

        role_output = "[ None ]"
        if member.roles:
            role_output = ""
            role_count = len(member.roles)
            for role in sorted(member.roles, key=lambda r: role_count - r.position)[:-1]:
                role_output += role.mention + "\n"

                if len(role_output) > 900:
                    embed.add_field(name="Roles [ {} ]".format(len(member.roles)), value=role_output, inline=False)
                    role_output = ""

        embed.add_field(name="Roles [ {} ]".format(len(member.roles)), value=role_output, inline=False)

        await context.send(embed=embed)

    @has_any_role("Mods", "Disaster Director")
    @commands.command(description="Give all the approved roles Trello Notification",
                      case_insensitive=True,
                      aliases=['annoy-staff', ])
    @has_any_role("Mods", "Disaster Director")
    async def trello_notif(self, context):
        trello_notif = get(context.guild.roles, name="Trello (Staff) Notification")
        all_approveds = set()
        roles = []
        for role in APPROVED_ROLES:
            approved_role = get(context.guild.roles, name=role)
            await context.send("Collecting Members for: {}".format(
                approved_role.name
            ))
            all_approveds.update(approved_role.members)

        await context.send("Adding Trello Notif role...")

        for approved in all_approveds:
            if trello_notif.name not in approved.roles:
                tmpl = "Giving {} Trello Notification!".format(approved.name)
                msg = await context.send(tmpl)
                await approved.add_roles(trello_notif)
                await msg.add_reaction("🌈")

    @commands.command(description="Returns all of the users that have a \
                                   specific role",
                      case_insensitive=True)
    @has_any_role('Mods', 'Disaster Director', 'Subreddit Moderator',
                  'Server Glue')
    async def rolemembers(self, context, roleArg=None):
        role_list = [role.name for role in context.message.guild.roles]
        if roleArg is None:
            await context.send("You didn't add a role.")
        else:
            newRole = get(context.guild.roles, name=roleArg)
            if newRole.name.lower() in [x.lower() for x in role_list]:
                memberNames = [x.mention for x in newRole.members]
                title = 'There is ' + str(len(newRole.members)) + ' Member(s) with this role.'
                colour = newRole.color
                embed = discord.Embed(title=title, colour=colour)
                field_value = ""
                field_amt = 0

                for member in memberNames:
                    if len(field_value) + len(member) >= 1000:
                        embed.insert_field_at(field_amt, name="Members with " + newRole.name, value=field_value,
                                              inline=True)
                        field_value = ""
                        field_amt += 1
                    field_value += "\n-" + member
                if field_value != "":
                    embed.insert_field_at(field_amt, name="Members with " + newRole.name, value=field_value,
                                          inline=True)
                await context.send(embed=embed)
            else:
                await context.send("That argument isn't in the rolelist.")

    @commands.command(description="Gives a brief description of what leveled permissions a user doesn't have",
                      case_insensitive=True)
    async def why_cant_i(self, context, user=None, filter=None):
        if context.message.mentions:
            user = context.message.mentions[0]
        else:
            filter = user
            user = context.author

        theoristRole = None
        theoristMention = None
        theoristNumber = 0
        theoristPerms = []
        theoristColor = user.top_role.color

        for dictRole in ROLE_PERMS:
            for role in user.roles:
                if role.name == dictRole["name"]:
                    theoristRole = role
                    theoristMention = role.mention
                    theoristColor = role.color
                    break
            if theoristRole == None:
                if isinstance(ROLE_PERMS[theoristNumber]["perms"], list):
                    for item in ROLE_PERMS[theoristNumber]["perms"]:
                        theoristPerms.insert(0, item)
                else:
                    theoristPerms.insert(0, ROLE_PERMS[theoristNumber]["perms"])
                theoristNumber += 1

        embed = discord.Embed(title="Why Can't I...? A Guide to Leveled Perms",
                              description="This command tells you what leveled permissions you can't currently use. For a more specific result, add a search term after v!why_cant_i.",
                              colour=theoristColor)
        embed.add_field(name="Your Role", value=theoristMention, inline=False)
        field_value = ""
        field_amt = 0

        if filter == None:
            for entry in theoristPerms:
                if len(field_value) + len(entry) >= 1024:
                    embed.insert_field_at(field_amt, name="Permissions", value=field_value, inline=False)
                    field_value = ""
                    field_amt += 1
                field_value += entry
            if field_value != "":
                embed.insert_field_at(field_amt, name="Permissions", value=field_value, inline=False)
            await context.send(embed=embed)
        else:
            filteredDesc = []
            for item in theoristPerms:
                if filter in item:
                    filteredDesc.append(item)
            if filteredDesc != []:
                for entry in filteredDesc:
                    if len(field_value) + len(entry) >= 1024:
                        embed.insert_field_at(0, name="Permissions", value=field_value)
                        field_value = ""
                    field_value += entry
                if field_value != "":
                    embed.insert_field_at(0, name="Permissions", value=field_value)
                await context.send(embed=embed)
            else:
                await context.send("There are no leveled permissions corresponding to that filter.")

    @commands.command(description="Gives a brief description of a given role. Role is case sensitive.",
                      case_insensitive=True)
    async def roleinfo(self, context, *, roleinput=None):
        if roleinput:
            roleinput = roleinput.replace('\"', '').replace('\'', '')
            role = get(context.guild.roles, name=roleinput)
            if role:
                embed = discord.Embed(title=f'{role}',
                                      description=f'''
                                        **ID**: <@{role.id}>
                                        **Color**: {role.color}''',
                                      colour=role.color)
                print(role.color)
                await context.send(embed=embed)
            else:
                await context.send("That role doesn't exist.")
        else:
            await context.send("You need to supply a role.")


def setup(bot):
    bot.add_cog(UtilitiesPlugin(bot))
